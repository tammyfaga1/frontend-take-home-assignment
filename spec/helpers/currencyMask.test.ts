import { currencyMask } from '../../src/helpers/currencyMask';

describe('currencyMask', () => {
  describe('when a string has only numbers', () => {
    const string = '222222';

    it('should return currency USD formart', () => {
      expect(currencyMask(string)).toEqual('2,222.22');
    });
  });

  describe('when a ampty string', () => {
    const string = '';

    it('should return 0 as float', () => {
      expect(currencyMask(string)).toEqual('0.00');
    });
  });

  describe('when numbers', () => {
    const number = 2500000;

    it('should return currency USD formart', () => {
      expect(currencyMask(number)).toEqual('25,000.00');
    });
  });

  describe('when a string has chars', () => {
    const string = 'ab12344';

    it('should return currency USD formart', () => {
      expect(currencyMask(string)).toBeNaN;
    });
  });
});
