import React from 'react';

import { ThemeProvider } from 'styled-components';
import { Normalize } from 'styled-normalize';
import { GlobalStyle, Theme } from './assets/styles/globalStyle';

import { Header } from './components/common/Header';
import { TakeHome } from './pages/TakeHome';

export const App: React.FC = () => {
  return (
    <ThemeProvider theme={Theme}>
      <Normalize />
      <GlobalStyle />
      <Header />
      <TakeHome />
    </ThemeProvider>
  );
};
