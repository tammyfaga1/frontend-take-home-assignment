export const currencyMask = (value: any) => {
  return new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD'
  })
    .format(value / 100)
    .replace('$', '');
};
