import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  body {
    -webkit-font-smoothing: antialiased !important;
    font-family: 'Work Sans', sans-serif;
    font-size: 16px;
    font-weight: 400;
    line-height: 120%;
  }
`;

export const Theme = {
  colors: {
    $neutralWhite: '#ffffff',
    $blueGray900: '#1e2a32',
    $brandColorPrimary: '#1b31a8',
    $brandColorSecondary: '#0079ff',
    $blueGray10: '#f4f8fA',
    $blueGray50: '#e9eef2',
    $blueGray400: '#708797',
    $blueGray600: '#4d6475'
  },
  fonts: {
    $primary: 'Work Sans, sans-serif',
    $secondary: 'Rubik, sans-serif'
  }
};
