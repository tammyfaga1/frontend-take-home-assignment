const size = {
  tablet: '768px',
  desktop: '1024px'
};

export const devices = {
  tablet: `(min-width: ${size.tablet})`,
  desktop: `(min-width: ${size.desktop})`
};
