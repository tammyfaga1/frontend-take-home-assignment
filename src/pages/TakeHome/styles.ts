import styled from 'styled-components';

import { devices } from '../../assets/styles/devices';

export const Container = styled.section`
  display: flex;
  flex-direction: column;
  height: calc(100vh - 56px);
  background-color: ${props => props.theme.colors.$blueGray10};

  @media ${devices.tablet} {
    height: calc(100vh - 80px);
  }
`;

export const Title = styled.h3`
  margin: 30px 10px;
  text-align: center;
  color: ${props => props.theme.colors.$brandColorPrimary};
  font-size: 18px;
  font-weight: 400;

  @media ${devices.tablet} {
    font-size: 20px;
  }
`;
