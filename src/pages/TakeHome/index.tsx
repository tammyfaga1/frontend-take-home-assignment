import React from 'react';

import { Container, Title } from './styles';
import { GoalCard } from '../../components/GoalCard';

import imgHouse from '../../assets/icons/house.svg';

export const TakeHome: React.FC = () => (
  <Container>
    <Title>
      Let{`'`}s plan your <b>saving goal.</b>
    </Title>
    <GoalCard imgSrc={imgHouse} title="Buy a house" />
  </Container>
);
