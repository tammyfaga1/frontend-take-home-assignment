import styled from 'styled-components';

import { devices } from '../../../../assets/styles/devices';

export const Result = styled.div`
  width: 100%;
  margin-bottom: 32px;
  border: ${props => `1px solid ${props.theme.colors.$blueGray50}`};
  box-sizing: border-box;
  border-radius: 8px;
`;

export const ResultHeader = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  padding: 27px 24px;
`;

export const ResultHeaderText = styled.div`
  font-size: 18px;
  color: ${props => props.theme.colors.$blueGray900};

  @media ${devices.tablet} {
    font-size: 20px;
  }
`;

export const ResultHeaderTotal = styled.div`
  font-family: ${props => props.theme.fonts.$secondary};
  font-size: 24px;
  font-weight: 500;
  color: ${props => props.theme.colors.$brandColorSecondary};

  @media ${devices.tablet} {
    font-size: 32px;
  }
`;

export const ResultFooter = styled.div`
  padding: 24px 30px;
  background-color: ${props => props.theme.colors.$blueGray10};
  border-bottom-left-radius: 8px;
  border-bottom-right-radius: 8px;
  font-size: 12px;
  text-align: center;
  line-height: 16px;
  color: ${props => props.theme.colors.$blueGray900};

  @media ${devices.tablet} {
    text-align: left;
  }
`;
