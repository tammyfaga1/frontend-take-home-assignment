import React, { FC } from 'react';

import {
  Result,
  ResultHeader,
  ResultHeaderText,
  ResultHeaderTotal,
  ResultFooter
} from './styles';

interface GoalResultsProps {
  totalAmount: string;
  monthlyAmount: string;
  quantityMonths: string | number;
  month: string;
  year: string | number;
}

export const GoalResults: FC<GoalResultsProps> = ({
  totalAmount,
  monthlyAmount,
  quantityMonths,
  month,
  year
}) => {
  return (
    <Result>
      <ResultHeader>
        <ResultHeaderText>Monthly amount</ResultHeaderText>
        <ResultHeaderTotal>${monthlyAmount}</ResultHeaderTotal>
      </ResultHeader>
      <ResultFooter>
        You’re planning <b>{quantityMonths} monthly deposits</b> to reach your
        <b> ${totalAmount}</b> goal by
        <b>
          {' '}
          {month} {year}.
        </b>
      </ResultFooter>
    </Result>
  );
};
