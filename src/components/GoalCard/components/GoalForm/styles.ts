import styled from 'styled-components';

import { devices } from '../../../../assets/styles/devices';

export const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

export const FormWrapper = styled.div`
  margin-bottom: 8px;

  @media ${devices.tablet} {
    display: grid;
    grid-template-columns: 272px 192px;
    grid-gap: 16px;
  }
`;
