import React, {
  FC,
  useState,
  useEffect,
  FormEvent,
  KeyboardEvent
} from 'react';
import { Button, TextField, DatePicker } from '../../../common';
import { GoalResults } from '../GoalResults';

import { currencyMask } from '../../../../helpers/currencyMask';

import { Form, FormWrapper } from './styles';

export const GoalForm: FC = () => {
  const today: Date = new Date();
  const todayMonth = today.toLocaleString('en-us', { month: 'long' });
  const todayYear = today.getFullYear();

  const [currentDate, setCurrentDate] = useState<Date>(today);
  const [year, setYear] = useState<number>(todayYear);
  const [month, setMonth] = useState<string>(todayMonth);
  const [totalAmount, setTotalAmount] = useState<number>(0);
  const [quantityMonths, setQuantityMonths] = useState<number>(1);
  const [monthlyAmount, setMonthlyAmount] = useState<number>(0);
  const [disableBtnPrev, setDisableBtnPrev] = useState<boolean>(true);

  const isJanuary = month === 'January';
  const istodayYear = year === todayYear;
  const istodayMonth = month === todayMonth;

  const formatedValue = totalAmount === 0 ? '' : currencyMask(totalAmount);

  useEffect(() => {
    if ((istodayYear && isJanuary) || (istodayYear && istodayMonth)) {
      setDisableBtnPrev(true);
    } else {
      setDisableBtnPrev(false);
    }
    setMonthlyAmount(totalAmount / quantityMonths);
  }, [
    year,
    month,
    totalAmount,
    istodayYear,
    isJanuary,
    istodayMonth,
    quantityMonths
  ]);

  const handleOper = (oper: 'acc' | 'des') => {
    const calc = oper === 'acc' ? +1 : -1;

    setCurrentDate(
      new Date(currentDate.setMonth(currentDate.getMonth() + calc))
    );
    setQuantityMonths(quantityMonths + calc);
    setMonth(currentDate.toLocaleString('en-us', { month: 'long' }));
  };

  const onDecrement = () => {
    if ((istodayYear && isJanuary) || (istodayYear && istodayMonth)) {
      setDisableBtnPrev(true);
    } else {
      handleOper('des');
    }

    !istodayYear && isJanuary && setYear(year - 1);
  };

  const onIncrement = () => {
    handleOper('acc');

    month === 'December' && setYear(year + 1);
  };

  const handleChange = (e: FormEvent<HTMLInputElement>) => {
    const value = e.currentTarget.value.replace(/\D/g, '');
    return setTotalAmount(Number(value));
  };

  const handleKeyBoard = (e: KeyboardEvent<HTMLDivElement>) => {
    if (e.key === 'ArrowLeft') {
      return onDecrement();
    } else if (e.key === 'ArrowRight') {
      return onIncrement();
    }

    return;
  };

  return (
    <Form>
      <FormWrapper>
        <TextField
          labelText="Total amount"
          value={formatedValue}
          onChange={handleChange}
          placeholder="0.00"
          autoComplete="off"
        />
        <DatePicker
          labelText="Reach goal by"
          month={month}
          year={year}
          disabled={disableBtnPrev}
          onKeyDown={handleKeyBoard}
          onDecrement={onDecrement}
          onIncrement={onIncrement}
        />
      </FormWrapper>
      <GoalResults
        totalAmount={currencyMask(totalAmount)}
        monthlyAmount={currencyMask(monthlyAmount)}
        quantityMonths={quantityMonths}
        month={month}
        year={year}
      />
      <Button onClick={e => e.preventDefault()} text="Confirm" />
    </Form>
  );
};
