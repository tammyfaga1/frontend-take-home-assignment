import React, { FC } from 'react';
import { GoalForm } from './components/GoalForm';

import {
  Container,
  Header,
  HeaderImage,
  HeaderWrapper,
  HeaderTitle,
  HeaderDescription
} from './styles';

interface GoalCardProps {
  imgSrc: string;
  title: string;
}

export const GoalCard: FC<GoalCardProps> = ({ imgSrc, title }) => {
  return (
    <Container>
      <Header>
        <HeaderImage src={imgSrc} />
        <HeaderWrapper>
          <HeaderTitle>{title}</HeaderTitle>
          <HeaderDescription>Saving goal</HeaderDescription>
        </HeaderWrapper>
      </Header>
      <GoalForm />
    </Container>
  );
};
