import styled from 'styled-components';

import { devices } from '../../assets/styles/devices';

export const Container = styled.div`
  padding: 24px;
  box-sizing: border-box;
  background-color: ${props => props.theme.colors.$neutralWhite};
  box-shadow: 0px 16px 32px rgba(30, 42, 50, 0.08);
  border-radius: 8px;

  @media ${devices.tablet} {
    width: 560px;
    margin: 0 auto;
    padding: 32px 40px;
  }
`;

export const Header = styled.div`
  display: flex;
  margin-bottom: 24px;
`;

export const HeaderImage = styled.img`
  max-width: 64px;
  height: auto;
  margin-right: 16px;
`;

export const HeaderWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const HeaderTitle = styled.h4`
  margin: 5px 0px;
  font-family: ${props => props.theme.fonts.$secondary};
  font-size: 20px;
  font-weight: 500;
  color: ${props => props.theme.colors.$blueGray900};

  @media ${devices.tablet} {
    font-size: 24px;
  }
`;

export const HeaderDescription = styled.p`
  margin: 5px 0px;
  font-size: 14px;
  line-height: 150%;
  color: ${props => props.theme.colors.$blueGray400};

  @media ${devices.tablet} {
    font-size: 16px;
  }
`;
