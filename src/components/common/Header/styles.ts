import styled from 'styled-components';

import { devices } from '../../../assets/styles/devices';

export const Container = styled.header`
  display: flex;
  max-width: 100%;
  padding: 16px;
  background-color: ${props => props.theme.colors.$neutralWhite};

  @media ${devices.tablet} {
    padding: 24px 56px;
  }
`;

export const Logo = styled.img`
  width: 75px;
  height: 24px;

  @media ${devices.tablet} {
    width: 100px;
    height: 32px;
  }
`;
