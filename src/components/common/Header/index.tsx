import React, { FC } from 'react';

import imgLogo from '../../../assets/icons/logo.svg';

import { Container, Logo } from './styles';

export const Header: FC = () => {
  return (
    <Container>
      <Logo
        src={imgLogo}
        alt="Origin Logo written in black"
        title="Origin Logo"
      />
    </Container>
  );
};
