import styled from 'styled-components';

import { devices } from '../../../assets/styles/devices';

export const StyledButton = styled.button`
  display: block;
  width: 100%;
  height: 56px;
  margin: 0 auto;
  padding: 18px 0;
  border: 0;
  border-radius: 32px;
  font-size: 16px;
  font-weight: 600;
  cursor: pointer;
  transition: all 0.25s cubic-bezier(0.02, 0.01, 0.47, 1);

  color: ${props => props.theme.colors.$neutralWhite};
  background-color: ${props => props.theme.colors.$brandColorPrimary};

  &:hover {
    box-shadow: 0 15px 15px rgba(0, 0, 0, 0.16);
    transform: translate(0, -5px);
  }

  @media ${devices.tablet} {
    width: 320px;
  }
`;
