import React, { FC, ButtonHTMLAttributes } from 'react';

import { StyledButton } from './styles';

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  text: string;
}

export const Button: FC<ButtonProps> = ({ text, ...otherProps }) => {
  return <StyledButton {...otherProps}>{text}</StyledButton>;
};
