import React, { FC, InputHTMLAttributes } from 'react';

import { FormInputValue, FormLabel, FormFieldWrapper } from './styles';

export interface TextFieldProps extends InputHTMLAttributes<HTMLInputElement> {
  labelText: string;
}

export const TextField: FC<TextFieldProps> = ({ labelText, ...otherProps }) => {
  return (
    <FormFieldWrapper>
      {labelText && <FormLabel>{labelText}</FormLabel>}
      <FormInputValue type="text" autoComplete="off" {...otherProps} />
    </FormFieldWrapper>
  );
};
