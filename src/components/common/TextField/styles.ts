import styled from 'styled-components';
import dollarSignIcon from '../../../assets/icons/dollar-sign.svg';
import { devices } from '../../../assets/styles/devices';

export const FormFieldWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const FormLabel = styled.label`
  margin-bottom: 5px;
  color: ${props => props.theme.colors.$blueGray900};
  font-size: 12px;
`;

export const FormInput = styled.input`
  height: 56px;
  width: 100%;
  margin-bottom: 16px;
  padding: 4px 14px;
  border: ${props => `1px solid ${props.theme.colors.$blueGray50}`};
  box-sizing: border-box;
  border-radius: 4px;
`;

export const FormInputValue = styled(FormInput)`
  padding-left: 44px;
  background-image: url(${dollarSignIcon});
  background-position: 12px center;
  background-repeat: no-repeat;
  font-family: ${props => props.theme.fonts.$secondary};
  font-size: 20px;
  font-weight: 500;
  color: ${props => props.theme.colors.$blueGray600};

  @media ${devices.tablet} {
    font-size: 24px;
  }
`;
