import React, { FC } from 'react';

import { FormLabel, FormFieldWrapper } from '../TextField/styles';
import { TextFieldProps } from '../TextField';

import {
  DatePickerContainer,
  DatePickerBtnPrev,
  DatePickerBtnNext,
  DatePickerContent,
  DatePickerMonth,
  DatePickerYear
} from './styles';

interface DatePickerProps extends TextFieldProps {
  month: string;
  year: string | number;
  disabled: boolean;
  onDecrement(): void;
  onIncrement(): void;
}

export const DatePicker: FC<DatePickerProps> = ({
  labelText,
  month,
  year,
  disabled,
  onDecrement,
  onIncrement,
  ...otherProps
}) => {
  return (
    <FormFieldWrapper>
      {labelText && <FormLabel>{labelText}</FormLabel>}
      <DatePickerContainer>
        <DatePickerBtnPrev
          type="button"
          disabled={disabled}
          onClick={onDecrement}
          {...otherProps}
        />
        <DatePickerContent>
          <DatePickerMonth>{month}</DatePickerMonth>
          <DatePickerYear>{year}</DatePickerYear>
        </DatePickerContent>
        <DatePickerBtnNext
          type="button"
          onClick={onIncrement}
          {...otherProps}
        />
      </DatePickerContainer>
    </FormFieldWrapper>
  );
};
