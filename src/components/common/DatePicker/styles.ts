import styled from 'styled-components';

import { devices } from '../../../assets/styles/devices';
import selectArrow from '../../../assets/icons/arrow.svg';

import { FormInput } from '../TextField/styles';

export const DatePickerContainer = styled(FormInput).attrs({ as: 'div' })`
  display: flex;
  justify-content: space-between;
`;

export const DatePickerContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
`;

export const DatePickerMonth = styled.span`
  font-size: 14px;
  font-weight: 600;
  line-height: 150%;
  text-align: center;
  color: ${props => props.theme.colors.$blueGray900};

  @media ${devices.tablet} {
    font-size: 16px;
  }
`;

export const DatePickerYear = styled.span`
  font-size: 14px;
  font-weight: 400;
  line-height: 150%;
  text-align: center;
  color: ${props => props.theme.colors.$blueGray400};

  @media ${devices.tablet} {
    font-size: 16px;
  }
`;

export const DatePickerBtnPrev = styled.input`
  display: block;
  height: 100%;
  width: 20px;
  background-image: url(${selectArrow});
  background-position: left;
  background-repeat: no-repeat;
  background-size: 10px;
  background-color: ${props => props.theme.colors.$neutralWhite};
  border: none;

  &:disabled {
    opacity: 0.4;
  }

  @media ${devices.tablet} {
    cursor: pointer;
    &:hover {
      background-size: 11px;
      transition: background-size 0.2s ease;
    }
  }
`;

export const DatePickerBtnNext = styled(DatePickerBtnPrev)`
  transform: rotate(180deg);
`;
